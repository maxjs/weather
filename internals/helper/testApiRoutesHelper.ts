import { createServer } from 'http';
import { NextApiHandler } from 'next';
import { apiResolver } from 'next/dist/next-server/server/api-utils';
import request from 'supertest';

export const testApiRoutesHelper = async (
  handler: NextApiHandler,
  { host = 'example.com' }: { host?: string } = {}
) =>
  request(
    createServer(async (req, res) => {
      req.headers.host = host;
      const matchApiParams = req?.url?.match(/api\/.*\/(.*)/);
      if (matchApiParams && matchApiParams.length > 1) {
        req.query = matchApiParams[1].split('/');
      }
      // mock for `apiResolver`'s 5th parameter to please TS
      const apiPreviewPropsMock = {
        previewModeId: 'id',
        previewModeEncryptionKey: 'key',
        previewModeSigningKey: 'key',
      };
      return apiResolver(
        req,
        res,
        undefined,
        handler,
        apiPreviewPropsMock,
        false
      );
    })
  );
