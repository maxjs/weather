# Weather App
This app shows the current weather for different cities. This is also my first try out with Next.js, so please be indulgent.

### Prerequisite

* Download the weather app source code here [or clone the repo](git@gitlab.com:maxjs/weather.git):

* Before you can run, you have to set your API key from [OpenWeatherMap.org](https://openweathermap.org/) within a file in your project root:

.env.local
```sh
OPEN_WEATHER_MAP_API_KEY=<api_key>
```

####Install nvm
Run following command in your terminal (or visit [nvm homepage](https://github.com/nvm-sh/nvm) for further instructions):
```
curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.38.0/install.sh | bash
```
####Install node.js
Run following command in your project root to make sure, you are using the right version.
```
nvm install
```
####Install pnpm
Run following command in your terminal:
```
npm i -g pnpm
```
Alternatively you can use npm as well of course ;)

##Install it and run:

```sh
pnpm install
pnpm dev
```

Visit [http://localhost:3000](http://localhost:3000)


##Future work
* adapt more to material design
* connect to CMS for content and user management
* i11n
* more tests. I implemented one test for the API routes (which was a little bit more complex)
* introduce Redux at some point (depends on how many features should be supported)
* make it more fancy
* adapt webpack configuration for production (chunking, etc.)
* use more static server-side generation
* add state persistency
* show more weather details (forecast for next 16 days etc.)
* show weather information on google maps
* dockerization
* better error handling  
* caching of weather data via proper tech (e.g. Cloudfront)  
* fixing ts errors
* dynamic ui update  
* code splitting
* improve accessibility  
* tabbing, settings  
* etc.