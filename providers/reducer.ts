import { ADD_CITIES, REMOVE_CITIES } from './constants';

export interface CitiesState {
  cities: Set<string>;
}

const reducer = (
  state: CitiesState,
  action: { type: string; payload: string[] }
): CitiesState => {
  const newState = { ...state };
  switch (action.type) {
    case ADD_CITIES:
      action.payload.forEach((city) => {
        newState.cities.add(city);
      });
      // eslint-disable-next-line no-case-declarations
      return newState;
    case REMOVE_CITIES:
      action.payload.forEach((city) => {
        newState.cities.delete(city);
      });
      return newState;
    default:
      return state;
  }
};

export default reducer;
