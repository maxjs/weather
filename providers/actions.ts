import { ADD_CITIES, REMOVE_CITIES } from './constants';

export const addCities = (...names: string[]): Record<string, unknown> => ({
  type: ADD_CITIES,
  message: 'Weather added',
  payload: names,
});

export const removeCity = (...names: string[]): Record<string, unknown> => ({
  type: REMOVE_CITIES,
  message: 'Weather removed',
  payload: names,
});
