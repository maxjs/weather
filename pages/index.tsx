import React from 'react';
import Layout from './Layout';
import WeatherContainer from '@/components/WeatherContainer';

import { useStore } from '../providers/Store';
import WeatherAppBar from '@/components/WeatherAppBar';

export default function Main() {
  const [state] = useStore();
  return (
    <Layout>
      <WeatherAppBar />

      <WeatherContainer cities={state.cities} />
    </Layout>
  );
}
