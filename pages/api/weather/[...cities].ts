// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import cacheItems from 'memory-cache';
import type { NextApiRequest, NextApiResponse } from 'next';
import OpenWeatherMap from 'openweathermap-ts';
import { CurrentResponse } from 'openweathermap-ts/dist/types';

const { OPEN_WEATHER_MAP_API_KEY, MEMORY_CACHE_MINUTES = '1' } = process.env;

if (!OPEN_WEATHER_MAP_API_KEY) {
  throw Error(
    'Please set a value for OPEN_WEATHER_MAP_API_KEY in your .env.local file!'
  );
}

const openWeather = new OpenWeatherMap({
  apiKey: OPEN_WEATHER_MAP_API_KEY,
  units: 'metric',
});

export const getWeatherItemsByCities = async (
  req: NextApiRequest,
  res: NextApiResponse
) => {
  //default for testing purposed, as dynamic param seems not to work when testing...
  const { cities } = req.query ?? { cities: ['London'] };
  const allWeatherItemsPromiseResults = await Promise.allSettled(
    (cities as string[]).map((city) => {
      const currentWeather = cacheItems.get(city);
      if (!currentWeather) {
        return openWeather
          .getCurrentWeatherByCityName({
            cityName: city as string,
          })
          .then((currentWeather) => {
            cacheItems.put(
              city,
              currentWeather,
              parseInt(MEMORY_CACHE_MINUTES) * 60
            );
            return Promise.resolve(currentWeather);
          });
      }
      return Promise.resolve(currentWeather);
    })
  );
  const allWeatherItems = allWeatherItemsPromiseResults.reduce(
    (acc, promiseResult) => {
      if (promiseResult.status === 'fulfilled') {
        acc.push(promiseResult.value);
      }
      return acc;
    },
    [] as Array<CurrentResponse>
  );
  res.status(200).json(allWeatherItems);
};

export default getWeatherItemsByCities;
