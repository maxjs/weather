const mockData = require('./weather.json');


export default function(){
  this.getCurrentWeatherByCityName = jest.fn(() => {
    return Promise.resolve(mockData);
  })
}