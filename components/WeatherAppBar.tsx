import React, { useRef } from 'react';
import {
  createStyles,
  fade,
  makeStyles,
  Theme,
} from '@material-ui/core/styles';
import { AppBar, IconButton, InputBase, Toolbar } from '@material-ui/core';

import AddIcon from '@material-ui/icons/Add';
import { useStore } from '../providers/Store';
import { addCities } from '../providers/actions';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    grow: {
      flexGrow: 1,
    },
    title: {
      display: 'none',
      [theme.breakpoints.up('sm')]: {
        display: 'block',
      },
    },
    search: {
      position: 'relative',
      borderRadius: theme.shape.borderRadius,
      backgroundColor: fade(theme.palette.common.white, 0.15),
      '&:hover': {
        backgroundColor: fade(theme.palette.common.white, 0.25),
      },
      marginLeft: 0,
      width: '100%',
    },
    inputRoot: {
      color: 'inherit',
    },
    inputInput: {
      padding: theme.spacing(1, 1, 1, 0),
      // vertical padding + font size from searchIcon
      paddingLeft: `1em`,
      transition: theme.transitions.create('width'),
      width: '100%',
    },
  })
);

export default function WeatherAppBar(): JSX.Element {
  const classes = useStyles();
  const [_, dispatch] = useStore();
  const cityInputRef = useRef();

  const addCityHandler = () => {
    const enteredCity = cityInputRef?.current?.value;
    dispatch(addCities(enteredCity));
    if (enteredCity) {
      // @ts-ignore
      cityInputRef.current.value = '';
    }
  };

  return (
    <div className={classes.grow}>
      <AppBar position="static">
        <Toolbar>
          <div className={classes.search}>
            <InputBase
              placeholder="City…"
              classes={{
                root: classes.inputRoot,
                input: classes.inputInput,
              }}
              inputRef={cityInputRef}
              fullWidth={true}
              onKeyPress={(e) => {
                if (e.code == 'Enter') {
                  addCityHandler();
                }
              }}
              inputProps={{ 'aria-label': 'enter city' }}
            />
          </div>
          <div className={classes.grow} />
          <div>
            <IconButton
              aria-label="add a weather card"
              color="inherit"
              onClick={addCityHandler}
            >
              <AddIcon />
            </IconButton>
          </div>
        </Toolbar>
      </AppBar>
    </div>
  );
}
