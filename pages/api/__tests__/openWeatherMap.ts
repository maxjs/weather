import http from 'http';
import fetch from 'isomorphic-unfetch';
import listen from 'test-listen';
import { apiResolver } from 'next/dist/next-server/server/api-utils';
import { getWeatherItemsByCities } from '../weather/[...cities]';

describe('open-weather-api handler', () => {
  test('responds 200 to authed GET', async () => {
    expect.assertions(1);
    const requestHandler = (req, res) => {
      res.user = { username: 'scooby' };
      return apiResolver(req, res, undefined, getWeatherItemsByCities);
    };
    const server = http.createServer(requestHandler);
    const url = await listen(server);
    const response = await fetch(url);
    expect(response.status).toBe(200);
    return server.close();
  });
});
