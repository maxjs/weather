import { getWeatherItemsByCities } from '../weather/[...cities]';
import { testApiRoutesHelper } from '../../../internals/helper/testApiRoutesHelper';

describe('open-weather-api handler', () => {
  test('responds 200 to authed GET', (done) => {
    expect.assertions(1);

    testApiRoutesHelper(getWeatherItemsByCities).then((client) =>
      client.get('/api/weather/London').then((response) => {
        expect(response.status).toBe(200);
        done();
      })
    );
  });
});
