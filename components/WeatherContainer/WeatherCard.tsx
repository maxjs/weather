import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardMedia from '@material-ui/core/CardMedia';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import { CurrentResponse } from 'openweathermap-ts/dist/types';
import { CardHeader } from '@material-ui/core';

const useStyles = makeStyles({
  root: {
    width: 150,
    height: 250,
    margin: '0.5rem',
    textAlign: 'center',
  },
  media: {
    height: 100,
    width: 100,
    margin: 'auto',
  },
  content: {
    margin: 'auto',
  },
});

interface WeatherCardProps {
  currentWeather: CurrentResponse;
}

const { NEXT_PUBLIC_OPEN_WEATHER_ICON_URL } = process.env;
const CONDITION = '%CONDITION';

export default function WeatherCard({
  currentWeather,
}: WeatherCardProps): JSX.Element {
  const classes = useStyles();

  const bestMatchCurrentWeather = currentWeather.weather[0];
  const weatherConditionIconUrl = NEXT_PUBLIC_OPEN_WEATHER_ICON_URL?.replace(
    CONDITION,
    bestMatchCurrentWeather.icon
  );

  return (
    <Card className={classes.root}>
      <CardHeader title={currentWeather.name} />
      <CardMedia
        className={classes.media}
        image={weatherConditionIconUrl}
        title={bestMatchCurrentWeather.main}
      />
      <Typography gutterBottom variant="h4" component="h2">
        {Math.round(currentWeather.main.temp)} &deg; C
      </Typography>
      <Typography gutterBottom variant="h6" component="h2">
        {bestMatchCurrentWeather.description}
      </Typography>

      <CardActions>
        <Button size="small" color="primary">
          Remove
        </Button>
      </CardActions>
    </Card>
  );
}
