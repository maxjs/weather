import React from 'react';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import GridList from '@material-ui/core/GridList';
import useSWR from 'swr';
import { CurrentResponse } from 'openweathermap-ts/dist/types';
import WeatherCard from '@/components/WeatherContainer/WeatherCard';

const { REFRESH_INTERVAL = '0' } = process.env;

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      marginTop: 8,
      display: 'flex',
      flexWrap: 'wrap',
      justifyContent: 'space-evenly',
      overflow: 'hidden',
      width: '100%',
      backgroundColor: theme.palette.background.paper,
    },
    icon: {
      color: 'rgba(255, 255, 255, 0.54)',
    },
  })
);

interface WeatherContainerProps {
  cities: Set<string>;
}

const fetcher = (url: string) => fetch(url).then((r) => r.json());

export default function WeatherContainer({
  cities,
}: WeatherContainerProps): JSX.Element {
  const classes = useStyles();
  const { data: allWeatherData, error } = useSWR(
    cities.size ? `/api/weather/${Array.from(cities).join('/')}` : null,
    fetcher,
    {
      refreshInterval: parseInt(REFRESH_INTERVAL),
    }
  );

  if (error) {
    console.error(error);
  }
  return (
    <div className={classes.root}>
      {allWeatherData ? (
        <GridList cellHeight={180} className={classes.gridList}>
          {(allWeatherData as Array<CurrentResponse>).map(
            (weatherData) =>
              weatherData.cod === 200 && (
                <WeatherCard
                  key={weatherData.id}
                  currentWeather={weatherData}
                />
              )
          )}
        </GridList>
      ) : (
        <h4>No entries so far!</h4>
      )}
    </div>
  );
}
