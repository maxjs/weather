import React, { createContext, FC, useContext, useReducer } from 'react';
import reducer, { CitiesState } from './reducer';

const initialState: CitiesState = {
  cities: new Set<string>(['Berlin', 'Waltham']),
};

const StoreContext = createContext<CitiesState>(initialState);

export const StoreProvider: FC = ({ children }) => {
  const [state, dispatch] = useReducer(reducer, initialState);

  return (
    <StoreContext.Provider value={[state, dispatch]}>
      {children}
    </StoreContext.Provider>
  );
};

export const useStore = () => useContext(StoreContext);
