import Head from 'next/head';
import React, { FC } from 'react';

interface LayoutProps {
  title: string;
  keywords: string;
  description: string;
}

const Layout: FC<LayoutProps> = ({
  title,
  keywords,
  description,
  children,
}) => {
  return (
    <div>
      <Head>
        <title>{title}</title>
        <meta name="description" content={description} />
        <meta name="keywords" content={keywords} />
      </Head>

      {children}
    </div>
  );
};

Layout.defaultProps = {
  title: 'Weather App',
  description: 'Check current weather for your locations',
  keywords: 'weather, openweathermap, forecast',
};

export default Layout;
