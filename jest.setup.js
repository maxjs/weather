const { configure } = require('enzyme');
const Adapter = require( 'enzyme-adapter-react-16');

process.env.OPEN_WEATHER_MAP_API_KEY = 'magicapikey';

configure({
  adapter: new Adapter()
});